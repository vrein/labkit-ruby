# frozen_string_literal: true

require "webrick"
require "rack"
require "httparty"
require "rest-client"
require "faraday"

describe Labkit::NetHttpPublisher do
  let(:rack_server) { TestRackServer.new(handler_proc) }
  before do
    described_class.labkit_prepend!
    rack_server.start
  end

  after do
    rack_server.stop
  end

  let(:successful_handler_proc) do
    ->(_req) {
      [
        200,
        { "Content-Type" => "application/json" },
        ['{"calculation" => 123}'],
      ]
    }
  end

  let(:successful_response) do
    {
      method: "GET", code: "200",
      scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
    }
  end

  let(:bad_request_handler_proc) do
    ->(_req) {
      [
        400,
        { "Content-Type" => "application/json" },
        ['{"error" => "calculation field is required"}'],
      ]
    }
  end

  let(:bad_request_response) do
    {
      method: "GET", code: "400",
      scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
    }
  end

  let(:server_error_handler) do
    ->(_req) { raise "something goes wrong" }
  end

  describe "requests made by Net/HTTP broadcast request.external_http events" do
    where(:request_proc, :handler_proc, :payloads) do
      [
        [
          -> {
            http = Net::HTTP.new("127.0.0.1", 9202)
            request = Net::HTTP::Get.new("/api/v1/tests")
            http.request(request)
          },
          successful_handler_proc, [successful_response],
        ],
        [
          -> {
            http = Net::HTTP.new("127.0.0.1", 9202)
            request = Net::HTTP::Get.new("http://127.0.0.1:9202/api/v1/tests")
            http.request(request)
          },
          successful_handler_proc, [successful_response],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202)
            connection.start do |http|
              request = Net::HTTP::Get.new("http://127.0.0.1:9202/api/v1/tests")
              http.request(request)
            end
          },
          successful_handler_proc, [successful_response],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202)
            connection.start do |http|
              http.get(URI("http://127.0.0.1:9202/api/v1/tests"))
            end
          },
          successful_handler_proc, [successful_response],
        ],
        [
          -> {
            Net::HTTP.get(URI("http://127.0.0.1:9202/api/v1/tests"))
          },
          successful_handler_proc,
          [successful_response],
        ],
        [
          -> {
            http = Net::HTTP.new("127.0.0.1", 9202)
            request = Net::HTTP::Get.new("/api/v1/tests")
            http.request(request)
          },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> {
            http = Net::HTTP.new("127.0.0.1", 9202)
            request = Net::HTTP::Get.new("http://127.0.0.1:9202/api/v1/tests")
            http.request(request)
          },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202)
            connection.start do |http|
              request = Net::HTTP::Get.new("http://127.0.0.1:9202/api/v1/tests")
              http.request(request)
            end
          },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202)
            connection.start do |http|
              http.get(URI("http://127.0.0.1:9202/api/v1/tests"))
            end
          },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> { Net::HTTP.get(URI("http://127.0.0.1:9202/api/v1/tests")) },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202)
            connection.start do |http|
              request = Net::HTTP::Get.new("http://127.0.0.1:9202/api/v1/tests?abc=1#xyz")
              request.basic_auth "username", "sensitvepassword"
              http.request(request)
            end
          },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1", fragment: "xyz",
            },
          ],
        ],
        [
          -> {
            Net::HTTP.get(URI("http://username:sensitvepassword@127.0.0.1:9202/api/v1/tests?abc=1#xyz"))
          },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1", fragment: "xyz",
            },
          ],
        ],
        [
          -> {
            Net::HTTP.get(URI("http://127.0.0.1:9202/this/is/a/not/found/path"))
          },
          successful_handler_proc,
          [
            {
              method: "GET", code: "404",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/this/is/a/not/found/path", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> { Net::HTTP.get(URI("http://127.0.0.1:9202/api/v1/tests")) },
          server_error_handler,
          [
            {
              method: "GET", code: "500",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> { Net::HTTP.post(URI("http://127.0.0.1:9202/api/v1/tests"), '{ "a" => 1 }') },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "POST", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202)
            connection.start do |http|
              http.head(URI("http://127.0.0.1:9202/api/v1/tests"))
            end
          },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "HEAD", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
      ]
    end

    with_them do
      it "broadcasts a correct notification" do
        begin
          subscriber = ActiveSupport::Notifications.subscribe("request.external_http") do |*args|
            event = ActiveSupport::Notifications::Event.new(*args)
            expect(event.name).to eq("request.external_http")
            expected_layload = payloads.shift # The shift here is to validate the number of notifications broadcasted as well
            duration = event.payload.delete(:duration)
            expect(duration).to be_a(Float).and(be > 0.0)
            expect(event.payload).to eql(expected_layload)
          end
          request_proc.call
        ensure
          ActiveSupport::Notifications.unsubscribe(subscriber) if subscriber
        end
      end
    end
  end

  describe "failed requests made by Net/HTTP broadcast request.external_http events" do
    context "with request timeout" do
      let(:handler_proc) do
        ->(_req) {
          sleep 0.1
          [
            200,
            { "Content-Type" => "application/json" },
            ['{"calculation" => 123}'],
          ]
        }
      end

      it "broadcasts an exception notification" do
        broadcasted = false
        begin
          subscriber = ActiveSupport::Notifications.subscribe("request.external_http") do |*args|
            broadcasted = true
            event = ActiveSupport::Notifications::Event.new(*args)
            expect(event.name).to eq("request.external_http")
            duration = event.payload.delete(:duration)
            expect(duration).to be_a(Float).and(be > 0.0)
            expect(event.payload).to include(
              method: "GET",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
              exception: ["Net::ReadTimeout", be_a(String)],
              exception_object: be_a(Net::ReadTimeout),
            )
          end
          http = Net::HTTP.new("127.0.0.1", 9202)
          http.read_timeout = 0.01
          http.start do
            request = Net::HTTP::Get.new("http://127.0.0.1:9202/api/v1/tests")
            http.request(request)
          end
        rescue Net::ReadTimeout
          # Ignored
        ensure
          ActiveSupport::Notifications.unsubscribe(subscriber) if subscriber
        end
        expect(broadcasted).to be(true)
      end
    end
  end

  describe "requests made by HTTP clients broadcast request.external_http events" do
    where(:request_proc, :handler_proc, :payloads) do
      [
        [
          -> { HTTParty.get("http://127.0.0.1:9202/api/v1/tests") },
          successful_handler_proc, [successful_response],
        ],
        [
          -> { HTTParty.get("http://127.0.0.1:9202/api/v1/tests", query: { abc: 1, xyz: 2 }) },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1&xyz=2", fragment: nil,
            },
          ],
        ],
        [
          -> { HTTParty.get("http://127.0.0.1:9202/api/v1/tests") },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> { HTTParty.get("http://127.0.0.1:9202/api/v1/tests") },
          server_error_handler,
          [
            {
              method: "GET", code: "500",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> { HTTParty.post("http://127.0.0.1:9202/api/v1/tests", { "a" => 1 }) },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "POST", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> { RestClient.get("http://127.0.0.1:9202/api/v1/tests") },
          successful_handler_proc, [successful_response],
        ],
        [
          -> { RestClient.get("http://127.0.0.1:9202/api/v1/tests", params: { abc: 1, xyz: 2 }) },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1&xyz=2", fragment: nil,
            },
          ],
        ],
        [
          -> { RestClient.get("http://127.0.0.1:9202/api/v1/tests") },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> { RestClient.get("http://127.0.0.1:9202/api/v1/tests") },
          server_error_handler,
          [
            {
              method: "GET", code: "500",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> { RestClient.post("http://127.0.0.1:9202/api/v1/tests", { "a" => 1 }) },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "POST", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],

        [
          -> { Faraday.get("http://127.0.0.1:9202/api/v1/tests") },
          successful_handler_proc, [successful_response],
        ],
        [
          -> { Faraday.get("http://127.0.0.1:9202/api/v1/tests", { abc: 1, xyz: 2 }) },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1&xyz=2", fragment: nil,
            },
          ],
        ],
        [
          -> { Faraday.get("http://127.0.0.1:9202/api/v1/tests") },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> { Faraday.get("http://127.0.0.1:9202/api/v1/tests") },
          server_error_handler,
          [
            {
              method: "GET", code: "500",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> { Faraday.post("http://127.0.0.1:9202/api/v1/tests", '{ "a" => 1 }') },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "POST", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
            },
          ],
        ],
        [
          -> {
            connection = Net::HTTP.new("127.0.0.1", 9202, "127.0.0.1", 9202)
            connection.start { |http| http.get("/api/v1/tests") }
          },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil, fragment: nil,
              proxy_host: "127.0.0.1", proxy_port: 9202,
            },
          ],
        ],
      ]
    end

    with_them do
      it "broadcasts a correct notification" do
        broadcasted = false
        begin
          subscriber = ActiveSupport::Notifications.subscribe("request.external_http") do |*args|
            broadcasted = true
            event = ActiveSupport::Notifications::Event.new(*args)
            expect(event.name).to eq("request.external_http")
            expected_layload = payloads.shift # The shift here is to validate the number of notifications broadcasted as well
            duration = event.payload.delete(:duration)
            expect(duration).to be_a(Float)
            expect(duration).to be > 0.0
            expect(event.payload).to eql(expected_layload)
          end
          request_proc.call
        rescue RestClient::RequestFailed
          # Ignored
        ensure
          ActiveSupport::Notifications.unsubscribe(subscriber) if subscriber
        end
        expect(broadcasted).to be(true)
      end
    end
  end
end
