# frozen_string_literal: true

describe Labkit::Middleware::Sidekiq::Tracing::Client do
  describe "#call" do
    let(:worker_class) { "test_worker_class" }
    let(:queue) { "test_queue" }
    let(:redis_pool) { double("redis_pool") }
    let(:span) { OpenTracing.start_span("test", ignore_active_scope: true) }

    subject { described_class.new }

    it "propagates exceptions" do
      custom_error = Class.new(StandardError)
      expect do
        subject.call(worker_class, { "class" => "jobclass", "queue" => "jobqueue", "retry" => 0, "args" => [] }, queue, redis_pool) do
          raise custom_error
        end
      end.to raise_error(custom_error)
    end

    context "when the job is scheduled directly" do
      let(:job) { { "class" => "jobclass", "queue" => "jobqueue", "retry" => 0, "args" => %w[1 2 3] } }

      it "yields" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing)
                                                   .with(
                                                     operation_name: "sidekiq:jobclass",
                                                     tags: {
                                                       "component" => "sidekiq",
                                                       "span.kind" => "client",
                                                       "sidekiq.queue" => "jobqueue",
                                                       "sidekiq.jid" => nil,
                                                       "sidekiq.retry" => "0",
                                                       "sidekiq.args" => "1, 2, 3",
                                                       "sidekiq.wrapped" => false,
                                                     },
                                                   ).and_yield(span)

        expect { |b| subject.call(worker_class, job, queue, redis_pool, &b) }.to yield_control
      end
    end

    context "when the job is scheduled via ActiveJob" do
      let(:job) do
        {
          "class" => "ActiveJob::QueueAdapters::SidekiqAdapter::JobWrapper",
          "queue" => "jobqueue",
          "retry" => 0,
          "args" => %w[1 2 3],
          "wrapped" => "jobclass",
        }
      end

      it "yields" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing)
                                                   .with(
                                                     hash_including(
                                                       operation_name: "sidekiq:jobclass",
                                                       tags: {
                                                         "component" => "sidekiq",
                                                         "span.kind" => "client",
                                                         "sidekiq.queue" => "jobqueue",
                                                         "sidekiq.jid" => nil,
                                                         "sidekiq.retry" => "0",
                                                         "sidekiq.args" => "1, 2, 3",
                                                         "sidekiq.wrapped" => true,
                                                       },
                                                     ),
                                                   ).and_yield(span)

        expect { |b| subject.call(worker_class, job, queue, redis_pool, &b) }.to yield_control
      end
    end

    context "when the job class is missing" do
      let(:job) { { "queue" => "jobqueue", "retry" => 0, "args" => %w[1 2 3] } }

      it "yields" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing)
                                                   .with(
                                                     operation_name: "sidekiq:undefined",
                                                     tags: {
                                                       "component" => "sidekiq",
                                                       "span.kind" => "client",
                                                       "sidekiq.queue" => "jobqueue",
                                                       "sidekiq.jid" => nil,
                                                       "sidekiq.retry" => "0",
                                                       "sidekiq.args" => "1, 2, 3",
                                                       "sidekiq.wrapped" => false,
                                                     },
                                                   ).and_yield(span)

        expect { |b| subject.call(worker_class, job, queue, redis_pool, &b) }.to yield_control
      end
    end

    context "when the job is scheduled in the future" do
      let(:job) do
        {
          "class" => "jobclass",
          "queue" => "jobqueue",
          "retry" => 0,
          "args" => %w[1 2 3],
          "at" => 1_234_567_890.123,
        }
      end

      it "yields" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing)
                                                   .with(
                                                     hash_including(
                                                       operation_name: "sidekiq:jobclass",
                                                       tags: {
                                                         "component" => "sidekiq",
                                                         "span.kind" => "client",
                                                         "sidekiq.queue" => "jobqueue",
                                                         "sidekiq.jid" => nil,
                                                         "sidekiq.retry" => "0",
                                                         "sidekiq.args" => "1, 2, 3",
                                                         "sidekiq.wrapped" => false,
                                                         "sidekiq.at" => 1_234_567_890.123,
                                                       },
                                                     ),
                                                   ).and_yield(span)

        expect { |b| subject.call(worker_class, job, queue, redis_pool, &b) }.to yield_control
      end
    end
  end
end
