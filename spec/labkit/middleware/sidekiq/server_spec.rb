# frozen_string_literal: true

require "sidekiq/testing"
require_relative "../../../support/sidekiq_middleware/shared_contexts"

describe Labkit::Middleware::Sidekiq::Server do
  class TestWorker
    include Sidekiq::Worker

    def perform(*args); end
  end

  let(:job) { { "class" => "TestWorker", "queue" => "default", "args" => ["hello"] } }

  include_context "with sidekiq server middleware setup"

  shared_examples "calling server middleware" do |middleware|
    it "calls the #{middleware} server middleware" do
      fake_middleware = double("middleware")
      expect(middleware).to receive(:new).and_return(fake_middleware)
      expect(fake_middleware).to receive(:call)
                                   .with(TestWorker, a_hash_including("args" => ["hello"]), "default")

      Sidekiq::Client.push(job)
    end
  end

  it_behaves_like "calling server middleware", Labkit::Middleware::Sidekiq::Context::Server

  context "when tracing is enabled" do
    before do
      described_class.instance_variable_set(:@chain, nil)
      allow(Labkit::Tracing).to receive(:enabled?).and_return(true)
    end

    it_behaves_like "calling server middleware", Labkit::Middleware::Sidekiq::Tracing::Server
  end

  context "setting caller_id" do
    it "sets caller_id to the job class" do
      expect(Labkit::Context).to receive(:with_context).with(a_hash_including("meta.caller_id" => "TestWorker"))

      Sidekiq::Client.push(job)
    end

    it "sets the caller_id to the wrapped job, if present" do
      expect(Labkit::Context).to receive(:with_context).with(a_hash_including("meta.caller_id" => "SomeOtherWorker"))

      Sidekiq::Client.push(job.merge("wrapped" => "SomeOtherWorker"))
    end
  end

  it "executes the job" do
    fake_job = TestWorker.new

    expect(TestWorker).to receive(:new).and_return(fake_job)
    expect(fake_job).to receive(:perform).with("hello")

    Sidekiq::Client.push(job)
  end
end
