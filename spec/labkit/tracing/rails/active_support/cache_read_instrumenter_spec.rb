require_relative "../../../../support/tracing/shared_examples"

describe Labkit::Tracing::Rails::ActiveSupport::CacheReadInstrumenter do
  using RSpec::Parameterized::TableSyntax

  where(:key, :hit, :super_operation) do
    nil | nil | nil
    123 | nil | nil
    123 | true | nil
    123 | false | nil
    123 | true | "fetch"
  end

  with_them do
    it_behaves_like "a tracing instrumenter" do
      let(:expected_span_name) { "cache_read" }
      let(:payload) { { key: key, hit: hit, super_operation: super_operation } }
      let(:expected_tags) do
        {
          "component" => "ActiveSupport",
          "cache.key" => key,
          "cache.hit" => hit,
          "cache.super_operation" => super_operation,
        }
      end
    end
  end
end
