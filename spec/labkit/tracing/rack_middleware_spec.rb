# frozen_string_literal: true

require "rack"

describe Labkit::Tracing::RackMiddleware do
  using RSpec::Parameterized::TableSyntax

  describe "#call" do
    context "when application is responding normally" do
      let(:fake_app) { ->(_env) { fake_app_response } }
      let(:request) { }

      subject { described_class.new(fake_app) }

      context "when returning 200 responses" do
        let(:fake_app_response) { [200, { :"Content-Type" => "text/plain" }, %w[OK]] }

        it "delegates correctly" do
          expect(subject.call(Rack::MockRequest.env_for("/"))).to eq(fake_app_response)
        end
      end

      context "when returning 500 responses" do
        let(:fake_app_response) { [500, { :"Content-Type" => "text/plain" }, %w[Error]] }

        it "delegates correctly" do
          expect(subject.call(Rack::MockRequest.env_for("/"))).to eq(fake_app_response)
        end
      end
    end

    context "when an application is raising an exception" do
      let(:custom_error) { Class.new(StandardError) }
      let(:fake_app) { ->(_env) { raise custom_error } }

      subject { described_class.new(fake_app) }

      it "delegates propagates exceptions correctly" do
        expect { subject.call(Rack::MockRequest.env_for("/")) }.to raise_error(custom_error)
      end
    end
  end

  describe ".build_sanitized_url_from_env" do
    def env_for_url(url)
      env = Rack::MockRequest.env_for(url)
      env["action_dispatch.parameter_filter"] = [/token/]

      env
    end

    where(:input_url, :output_url) do
      "/gitlab-org/gitlab-ce" | "http://example.org/gitlab-org/gitlab-ce"
      "/gitlab-org/gitlab-ce?safe=1" | "http://example.org/gitlab-org/gitlab-ce?safe=1"
      "/gitlab-org/gitlab-ce?private_token=secret" | "http://example.org/gitlab-org/gitlab-ce?private_token=%5BFILTERED%5D"
      "/gitlab-org/gitlab-ce?mixed=1&private_token=secret" | "http://example.org/gitlab-org/gitlab-ce?mixed=1&private_token=%5BFILTERED%5D"
    end

    with_them { it { expect(described_class.build_sanitized_url_from_env(env_for_url(input_url))).to eq(output_url) } }
  end
end
