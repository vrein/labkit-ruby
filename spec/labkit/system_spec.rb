# frozen_string_literal: true

describe Labkit::System do
  describe ".monotonic_time" do
    it "returns a Float" do
      expect(described_class.monotonic_time).to be_a(Float)
    end
  end
end
