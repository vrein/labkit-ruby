# frozen_string_literal: true

require "opentracing"

RSpec.shared_examples "a tracing instrumenter" do
  subject(:instrumenter) { described_class.new }

  let(:fake_span) { double("OpenTracing span") }
  let(:fake_scope) { double("OpenTracing scope", span: fake_span) }
  let(:sampled) { true }

  before do
    instrumenter.scope_stack.clear

    allow(Labkit::Tracing).to receive(:sampled?).and_return(sampled)
  end

  describe "#start" do
    it "starts an Opentracing span with the correct name" do
      expect(OpenTracing).to receive(:start_active_span).with(expected_span_name)

      instrumenter.start("event name", "id", payload)
    end

    it "adds the span to the active stack" do
      allow(OpenTracing).to receive(:start_active_span).and_return(fake_scope)

      instrumenter.start("event name", "id", payload)

      expect(instrumenter.scope_stack).to contain_exactly(fake_scope)
    end
  end

  describe "#finish" do
    before do
      instrumenter.scope_stack.clear
      instrumenter.scope_stack << fake_scope
    end

    it "closes the span" do
      allow(fake_span).to receive(:set_tag)
      allow(fake_span).to receive(:log_kv)

      expect(fake_scope).to receive(:close)

      instrumenter.finish("event name", "id", payload)
    end

    it "sets the correct tags" do
      expected_tags.each do |name, value|
        expect(fake_span).to receive(:set_tag).with(name, value)
      end
      allow(fake_span).to receive(:set_tag)
      allow(fake_span).to receive(:log_kv)

      allow(fake_scope).to receive(:close)

      instrumenter.finish("event name", "id", payload)
    end

    it "logs an exception on the span when payload has exception field" do
      allow(fake_span).to receive(:set_tag)

      expect(fake_span).to receive(:set_tag).with("error", true)
      expect(fake_span).to receive(:log_kv).with(
        :"event" => "error",
        :"error.kind" => "StandardError",
        :"message" => "something goes wrong",
        :"stack" => nil,
      )

      allow(fake_scope).to receive(:close)

      instrumenter.finish("event name", "id", payload.merge(exception: StandardError.new("something goes wrong")))
    end

    it "logs an exception on the span when payload has exception_object field" do
      allow(fake_span).to receive(:set_tag)

      expect(fake_span).to receive(:set_tag).with("error", true)
      expect(fake_span).to receive(:log_kv).with(
        :"event" => "error",
        :"error.kind" => "StandardError",
        :"message" => "something goes wrong",
        :"stack" => nil,
      )

      allow(fake_scope).to receive(:close)

      instrumenter.finish("event name", "id", payload.merge(exception_object: StandardError.new("something goes wrong")))
    end
  end
end
